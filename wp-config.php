<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'vinuni_website' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'oWdF>EE63S;lzh4D*t*89<~K1AC5)E+`MhS>W4iYnae@dLQ$}6GO}x0Z^T8yx2Mu' );
define( 'SECURE_AUTH_KEY',  'y HR}YXWPIct0GY5(ZG>-f_gZuUtPD75?/#{9=:lM1S%%{12i+v4S)kLOLJC8MSx' );
define( 'LOGGED_IN_KEY',    '|J>)O^z^Tp;ON_CZke;zG8esGui5SG^%|U%xM/l 2u^XEl)=b[e;5RqZg%<NpJMO' );
define( 'NONCE_KEY',        'ppl+JyAJ)`!&7gV=d)lPq2d%(pIV,@kShl^G%|AeXTs:PYw{fEbR[}jlsU4Kom E' );
define( 'AUTH_SALT',        'o.]~>ZpHVT<rxHf|.GD_E CQ,F/kS8Sn-9}*8^4!+f0yJS?M ;|nnqYtu7Au$ j0' );
define( 'SECURE_AUTH_SALT', 't0US%Y`ozSiS5sD3(SA[fx8=8B]nERf&)T[I$*V0/sTxoX~!qUvf=-?g<W&zL4xM' );
define( 'LOGGED_IN_SALT',   '-de>O]$;P1.pPVtLA]69_MFdhq&Eyt~l,c$LV**oE_poj&S8=gLS`*&sllL~{Q|K' );
define( 'NONCE_SALT',       'QSv:9ViL]K~q>,Ma!ZsBwmb75Jq^Ka 5Jm%Q>(qYF?Jfn`4}-S%*s}P+1Jjm~-2V' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
