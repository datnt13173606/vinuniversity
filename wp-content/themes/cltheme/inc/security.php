<?php
/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function tls_remove_wp_version_strings( $src ) {
    global $wp_version;
    parse_str(parse_url($src, PHP_URL_QUERY), $query);
    if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}
add_filter( 'script_loader_src', 'tls_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'tls_remove_wp_version_strings' );
/* Hide WP version strings from generator meta tag */
function tls_remove_version() {
    return '';
    }
add_filter('the_generator', 'tls_remove_version');
/* Disable XML-RPC */
add_filter('xmlrpc_enabled', '__return_false');
/* Set cookie timeout to 15 minutes */
add_filter('auth_cookie_expiration', 'tls_expiration_filter', 99, 3);
function tls_expiration_filter($seconds, $user_id, $remember){

    //if "remember me" is checked;
    if ( $remember ) {
        //WP defaults to 2 weeks;
        $expiration = 14*24*60*60; //UPDATE HERE;
    } else {
        //WP defaults to 48 hrs/2 days;
        $expiration = 15*60; //UPDATE HERE;
    }

    //http://en.wikipedia.org/wiki/Year_2038_problem
    if ( PHP_INT_MAX - time() < $expiration ) {
        //Fix to a little bit earlier!
        $expiration =  PHP_INT_MAX - time() - 5;
    }

    return $expiration;
}

// remove wp version number from scripts and styles
function cl_remove_css_js_version( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'cl_remove_css_js_version', 1 );
add_filter( 'script_loader_src', 'cl_remove_css_js_version', 1 );

// remove wp version number from head and rss
function cl_remove_version() {
    return '';
}
add_filter('the_generator', 'cl_remove_version');