<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cltheme
 */

?>

	<footer class="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-4">
						<div class="d-flex mb-3">
							<a class="logo-ft" href="<?php echo get_home_url(); ?>">
								<img class="img-fluid" width="90" height="76" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-ft.png" alt="<?php echo get_option('blogname'); ?>">
							</a>
						</div>
					</div>
					<div class="col-12 col-lg-4">
						<div class="wg-ft">
							<h5 class="h6 mb-3"><?php echo __('Contact Us', 'cltheme'); ?></h5>
							
							<div class="wg-ft mb-4">
								<?php
								wp_nav_menu(
									array(
										'theme_location' => 'menu-footer-1',
										'menu_class' => 'no-bullets',
										'container' => ''
									)
								);
								?>
							</div>
						</div>
					</div>
					<div class="col-12 col-lg-4">
						<div class="wg-ft">
							<h5 class="h6 mb-3"><?php echo __('Follow Us ', 'cltheme'); ?></h5>
							
							<div class="wg-ft mb-4">
								<?php
								wp_nav_menu(
									array(
										'theme_location' => 'menu-footer-2',
										'menu_class' => 'no-bullets',
										'container' => ''
									)
								);
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
				<div class="copyright-content">
					<p>
						<?php
						echo __('Copyright © 2022. All right reserved ', 'cltheme');
						?>
					</p>
				</div>
			</div>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
