<?php

/**
 * Template Name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package vinuni
 */

get_header();
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</head>

<body>

    <!-- Header -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="<?php echo get_template_directory_uri() . '/assets/images/vinuni_logo.png'; ?>" alt=""
                    class="d-inline-block align-text-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">About The Summit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Speakers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Schedule</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Excellence Awards</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Invited Sesssion</a>
                    </li>
                    <button class="btn btn-outline-danger" type="submit">Login</button>
                    <button class="btn btn-outline-danger" type="submit">Register Now</button>
                </ul>
            </div>
        </div>
    </nav>


    <!-- Section Conference  -->
    <section class="section_conference">
        <div class="container">
            <div class="row pb-80">
                <div class="col-lg-6 left text-white">
                    <h6 class="title">12-14 MAY 2022, VINUNIVERSITY CAMPUS</h6>
                    <h1 class="subtitle">VINUNIVERSITY TEACHING AND LEARNING CONFERENCE 2022</h1>
                    <h2 class="description">Co-Creating Learning for the Future</h2>
                    <h6 class="content">“If we teach today’s students as we taught yesterday’s, we rob them of
                        tomorrow.” ― John Dewey</h6>
                    <div class="col-lg-12">
                        <a class="btn btn-danger" href="#" role="button">Register</a>
                        <a class="btn btn-outline-danger" href="#" role="button">See the schedule</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="bg_image">
            <img src="<?php echo get_template_directory_uri() . '/assets/images/img_conference.png'; ?>"
                alt="Logo Vin Uni">
        </div>


    </section>


    <!-- Section Summit Theme-->
    <section class="section_summit">
        <div class="container">
            <div class="row pb-80">
                <div class="col-lg-12 content text-center">
                    <h2 class="main__title">SUMMIT THEMES</h2>
                    <p class="line__solid"></p>
                    <h6 class="summit__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aliquam
                        tristique sed posuere elementum amet. Feugiat sed arcu risus lacus.</h6>
                </div>
                <div class="subcontent text-white">
                    <div class="col-md-12 box-info">
                        <p class="text-box">Co-Creating Learning Experiences through Student Partnerships</p>
                    </div>
                    <div class="box-info">
                        <p class="text-box"> Active, Experiential & Technology Enhanced Learning</p>
                    </div>
                    <div class="box-info">
                        <p class="text-box">Leave No Student Behind: Inclusive Teaching Practices</p>
                    </div>
                </div>
            </div>
        </div>

    </section>




    <!-- Section Our Speakers -->
    <section class="section_our-speaker">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 content text-center">
                    <h3 class="main__title">Our SpeakerS</h3>
                    <p class="line__solid"></p>
                </div>
                <p class="subtitle">Keynote speakers</p>
            </div>
            <div class="row pb-80">
                <div class="col-lg-4 profile">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/profile.png'; ?>" alt=""
                        class="profile__img">
                    <div class="profile__content text-center">
                        <h6 class="profile__name">Darlene Robertson</h6>
                        <h6 class="profile__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h6>
                        <p href="" class="text__more">
                            View more
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/arrow-right.png'; ?>"
                                alt="" class="text__more-img">
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 profile">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/profile2.png'; ?>" alt=""
                        class="profile__img">
                    <div class="profile__content text-center">
                        <h6 class="profile__name">Darlene Robertson</h6>
                        <h6 class="profile__description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h6>
                        <p href="" class="text__more">
                            View more
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/arrow-right.png'; ?>"
                                alt="" class="text__more-img">
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 our__content">
                    <h6 class="our__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit
                        amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim
                        praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam
                        dignissim diam quis</h6>
                    <a class="btn btn-outline-danger" href="#" role="button">View All</a>
                </div>
            </div>
        </div>
    </section>


    <!-- Section Call for Proposals -->
    <section class="section_proposals">
        <div class="container">
            <div class="row pb-80">
                <div class="col-lg-7">
                    <h3 class="main__title">CALL FOR PROPOSALS</h3>
                    <p class="line__solid"></p>
                    <div class="content__proposals">
                        <p class="title__proposals">We welcome contributions from educators and students to the
                            summit. Your session can follow one of the many formats listed below. If you are not sure,
                            you can select multiple formats and we will contact you to select the most suitable one. To
                            submit more than one proposal, use separate forms.</p>
                        <p class="subtitle__proposals">If you would like to Chair a session, please indicate that in
                            your response.</p>
                        <h6 class="title_deadline">Proposal Deadline: April 15, 2022.</h6>
                    </div>
                    <a href="" class="btn btn-danger">Submit your proposal</a>
                </div>

                <div class="col-lg-5">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/proposals.png'; ?>" alt=""
                        class="img__proposals">
                </div>
            </div>
        </div>
    </section>



    <!-- Section Summit agenda -->
    <section class="section_agenda">
        <div class="container">
            <div class="row pb-80">
                <div class="col-lg-12 text-center">
                    <h3 class="main__title">SUMMIT AGENDA</h3>
                    <p class="line__solid"></p>
                    <p class="agenda__title">Investing in the Future of Education in Vietnam: Invited discussion with
                        MOET officials, university and school- teachers and administrators on challenges facing school
                        and college education in Vietnam and development of concrete initiatives to support innovative
                        practices.</p>
                    <a href="" class="btn btn-danger">View More</a>
                </div>
            </div>
        </div>
    </section>


    <!-- Teaching Exellence Awards -->
    <section class="section_teaching">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="main__title">Teaching Exellence Awards</h3>
                    <p class="line__solid"></p>
                </div>
            </div>
            <div class="content pb-80">
                <div class="row">
                    <div class="col-lg-4">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/awards.png'; ?>"
                            alt="img teaching awards">
                    </div>
                    <div class="col-lg-8">
                        <p class="teaching_notify">
                            We are delighted to announce a Teaching Excellence Awards program for innovative and
                            effective
                            teachers in Higher Education in Vietnam. These awards will recognize the outstanding impact
                            of
                            educators who exemplify excellence in teaching.
                            <br /><br />
                            Nominations must be submitted online using the form below. All valid nominations will be
                            reviewed by a committee based on set criteria. The review committee and the criteria will be
                            published shortly.
                        </p>
                        <p class="teaching_condition">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/check-circle.png'; ?>"
                                alt="img check circle">
                            Up to five (5) teachers and two (2) teacher teams will be awarded teaching excellence
                            awards. Award recipients receive $500 towards their professional development fund.
                            <br /><br />
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/check-circle.png'; ?>"
                                alt="img check circle">
                            Nominations are invited from educators, students, and school/college administrators.
                        </p>
                        <h6 class="title_deadline">
                            Nomination Deadline: April 15, 2022.
                        </h6>
                        <a href="" class="btn btn-danger">Nominate An Excellent Educator</a>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <!-- Registration -->
    <section class="section_registration">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/registration.png';  ?>"
                        alt="registration">
                </div>
                <div class="col-lg-7 text-right text-white">
                    <h3 class="main__title text-white">REGISTRATION</h3>
                    <p class="registration_title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dignissim
                        lacus nunc dolor consectetur aenean libero id. Aliquet quam phasellus id scelerisque id nisl.
                    </p>
                    <a href="" class="btn btn-outline-danger">Register Your Interest Now</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Considerations -->
    <section class="section_considerations">
        <div class="container">
            <div class="row pb-80">
                <div class="col-lg-12 text-center">
                    <h3 class="main__title">COVID 19 CONSIDERATIONS </h3>
                    <p class="line__solid"></p>
                    <p class="consider__title text-left">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/check-circle.png'; ?>"
                            alt="img check circle">
                        If the Government rules allow, all events will be held on the VinUniversity campus, Ocean Park,
                        Gia Lam District, Hanoi. Some events may be held online as needed.
                        </br><br />
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/check-circle.png'; ?>"
                            alt="img check circle">
                        If COVID restrictions continue, the summit will be held online.
                    </p>
                </div>
            </div>
        </div>
    </section>





    <!-- Footer -->
    <footer>
        <div class="footer__top">
            <div class="container">
                <div class="row gy-4 gx-5">
                    <div class="col-lg-3 col-md-6">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/logo_footer.png'; ?>"
                            alt="Logo Footer" class="logo_footer">
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" class="text-white">
                                    Speakers
                                </a>
                            </li>
                            <li>
                                <a href="#" class="text-white">
                                    Schedule
                                </a>
                            </li>
                            <li>
                                <a href="#" class="text-white">
                                    About the summit
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 text-white">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#" class="text-white">
                                    The Excellence Awards
                                </a>
                            </li>
                            <li><a href="#" class="text-white">
                                    Invited session
                                </a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <h5 class="text-white h6">Contact</h5>
                        <p class="text-white">VinUniversity, Vinhomes Ocean Park, Gia Lam District, Hanoi.</p>
                        <h6 class="text-contact text-white">support@example.com </h6>
                        <h6 class="text-contact text-white">(84) 555-0120-111 </h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <p class="text-white text-center">Copyright © 2022 VinUni. All right reserved</p>
            </div>
        </div>
    </footer>
</body>

</html>

<?php
get_footer();