<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cltheme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php //body_class(); 
        ?>>
    <?php wp_body_open(); ?>
    <div id="page-wrapper" class="site">
        <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'cltheme'); ?></a>

        <header class="header <?php /* if( is_front_page() ) { echo 'header-home'; } */ ?>" id="site-header">
            <div class="container">
                <div class="header-left">
                    <div class="header-brand">
                        <?php
                        // $src_custom_logo = esc_url( wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' )[0] ); 
                        $src_uri_logo = get_template_directory_uri() . '/assets/images/logo.png';
                        ?>
                        <?php if (is_front_page() && is_home()) : ?>
                            <h1 class="h4 mb-0">
                                <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                    <img class="img-fluid" src="<?php echo $src_uri_logo; ?>" height="81" width="92" alt="<?php echo get_option('blogname'); ?>">
                                </a>
                            </h1>
                        <?php else : ?>
                            <p class="h4 mb-0">
                                <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                    <img class="img-fluid" src="<?php echo $src_uri_logo; ?>" height="81" width="92" alt="<?php echo get_option('blogname'); ?>">
                                </a>
                            </p>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="header-right">
                    <div id="hamburger-menu"><span class="bar bar1"></span><span class="bar bar2"></span><span class="bar bar3"></span><span class="bar bar4"></span></div>
                    <div class="header-nav">
                        <?php
                        if (has_nav_menu('menu-main')) {
                            wp_nav_menu(
                                array(
                                    'theme_location'  => 'menu-main',
                                    'container'       => 'nav',
                                    'container_class' => 'container',
                                    'menu_class'      => 'menu'
                                )
                            );
                        }
                        ?>
                    </div>
                </div>
            </div>
        </header><!-- #masthead -->